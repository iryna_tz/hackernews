﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerNews.Controllers;
using System.Collections.Generic;
using HackerNews.Models;

namespace HackerNews.Tests.Controllers
{
    [TestClass]
    public class HNewsWebAPIControllerTest
    {
        [TestMethod]
        public void Get()
        {
            var controller = new HNewsWebAPIController();
            LinkedList<ItemDetail> list = controller.Get();
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void CacheGet()
        {
            var controller = new HNewsWebAPIController();
            LinkedList<ItemDetail> list = controller.Get();
            Assert.IsTrue(list.First.Value.Equals(controller.getCachedItem(list.First.Value.id)));
        }
    }
}
