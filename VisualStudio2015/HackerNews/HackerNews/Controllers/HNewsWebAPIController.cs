﻿using HackerNews.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web.Http;


namespace HackerNews.Controllers
{
    public class HNewsWebAPIController : ApiController
    {
        private static MemoryCache cache = MemoryCache.Default;

        public void addToCache(string id, ItemDetail content)
        {
            var cacheItem = new CacheItem(id);
            cacheItem.Value = content;
            var cachePolicy = new CacheItemPolicy();
            cache.Add(cacheItem, cachePolicy);
        }

        public ItemDetail getCachedItem(string id)
        {
            return (ItemDetail)cache.Get(id);
        }

        public LinkedList<ItemDetail> Get()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://hacker-news.firebaseio.com/");
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("v0/beststories.json").Result;
            var task = response.Content.ReadAsStringAsync();
            var jsonContent = task.Result;

            var bestStoriesList = JsonConvert.DeserializeObject<List<string>>(jsonContent);

            var detailList = new LinkedList<ItemDetail>();

            foreach (string id in bestStoriesList.GetRange(0,10))
            {
                detailList.AddLast(Get(id));
            }
            return detailList;
        }

        public ItemDetail Get(String id)
        {
            ItemDetail itemDetaii = getCachedItem(id);
            if(itemDetaii == null)
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("https://hacker-news.firebaseio.com/");
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync($"v0/item/{id}.json").Result;
                var task = response.Content.ReadAsStringAsync();
                itemDetaii = JsonConvert.DeserializeObject<ItemDetail>(task.Result);
                addToCache(id, itemDetaii);
            }
            return itemDetaii;
        }
    }
}
