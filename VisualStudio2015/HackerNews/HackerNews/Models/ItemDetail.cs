﻿namespace HackerNews.Models
{
    public class ItemDetail
    {
        public string id { get; set; }

        public string by { get; set; }

        public string title { get; set; }

    }
}