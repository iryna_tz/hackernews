﻿(function () {
   'use strict';
    var app = angular.module('hackerNews', []);

    app.service('HNewsWebAPIService', function ($http) {
        var getBestStories = function () {
            return $http({
                method: 'GET',
                url: 'api/HNewsWebAPI'
            });
        };

        var getStoryDetail = function (id) {
            return $http({
                method: 'GET',
                url: 'api/HNewsWebAPI',
                params: { id: id }
            });
        };

        return {
            getBestStories: getBestStories,
            getStoryDetail: getStoryDetail
        }
    });
    
    app.controller('HNewsWebAPIController', function (HNewsWebAPIService,$scope) {
        HNewsWebAPIService.getBestStories()
        .then(function successCallback(response) {
            $scope.stories=angular.fromJson(response.data);
        }, function errorCallback(response) {
            console.error('Error');
        });
    });

})()


